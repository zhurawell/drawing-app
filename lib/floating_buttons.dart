import 'package:drawing_app/utilities.dart';
import 'package:flutter/material.dart';

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:drawing_app/drawing_page.dart';
import 'package:drawing_app/resources/app_colors.dart';
import 'package:drawing_app/resources/app_icons.dart';

class FloatingButtons extends ConsumerWidget {
  const FloatingButtons({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    bool magnetize = ref.watch(magnetProdiver);

    Widget gridButton() {
      return ElevatedButton(
        style: ElevatedButton.styleFrom(
          fixedSize: Size.fromHeight(150.sp),
          shape: const CircleBorder(),
          backgroundColor: magnetize ? AppColors.blueLight : AppColors.white,
          surfaceTintColor: AppColors.transparent,
        ),
        onPressed: () {
          List<Offset> magnetedPoints = [];
          if (!magnetize) {
            for (Offset point in ref.read(drawingProvider.notifier).state) {
              magnetedPoints.add(point.round(magnetStep));
            }
            ref.read(drawingProvider.notifier).state = magnetedPoints;
          }

          ref.read(magnetProdiver.notifier).state = !magnetize;
        },
        child: Icon(
          AppIcons.grid,
          color: magnetize ? AppColors.white : AppColors.grayDark,
        ),
      );
    }

    Widget redoButton() {
      return IconButton(
        highlightColor: AppColors.transparent,
        onPressed: () {},
        icon: Icon(
          AppIcons.redo,
          color: AppColors.grayDark,
          size: 60.sp,
        ),
      );
    }

    Widget undoButton() {
      return IconButton(
        highlightColor: AppColors.transparent,
        onPressed: () {},
        icon: Icon(
          AppIcons.undo,
          color: AppColors.grayDark,
          size: 60.sp,
        ),
      );
    }

    return Padding(
      padding: EdgeInsets.fromLTRB(40.sp, 60.sp, 20.sp, 0.sp),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            height: 100.sp,
            width: 300.sp,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(25.sp)),
              color: AppColors.white,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                undoButton(),
                VerticalDivider(
                  color: AppColors.grayDark,
                  width: 5.sp,
                ),
                redoButton(),
              ],
            ),
          ),
          gridButton(),
        ],
      ),
    );
  }
}
