import 'package:flutter/material.dart';

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:drawing_app/initialize.dart';
import 'package:drawing_app/drawing_page.dart';

void main() async {
  await initialize();

  runApp(const MainApp());
}

class MainApp extends ConsumerWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) => ScreenUtilInit(
        designSize: const Size(1080, 2400),
        minTextAdapt: true,
        splitScreenMode: true,
        // Решение неполадок с Flutter 3.19.5 и flutter_screentuil 5.9.0 ↓
        fontSizeResolver: (fontSize, instance) {
          final display = View.of(context).display;
          final screenSize = display.size / display.devicePixelRatio;
          final scaleWidth = screenSize.width / const Size(1080, 2400).width;

          return fontSize * scaleWidth;
        },
        builder: (context, child) => const ProviderScope(
          child: MaterialApp(
            title: 'Drawing App',
            debugShowCheckedModeBanner: false,
            home: SafeArea(child: DrawingPage()),
          ),
        ),
      );
}
