import 'package:drawing_app/resources/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class DrawingPainter extends CustomPainter {
  DrawingPainter(this.points);
  final List<Offset> points;

  final Path path = Path();

  final Paint _line = Paint()
    ..color = AppColors.black
    ..strokeCap = StrokeCap.round
    ..strokeWidth = 15.sp;

  final Paint _path = Paint()..color = AppColors.white;

  final Paint _circleOuter = Paint()
    ..color = AppColors.white
    ..strokeWidth = 2.sp;
  final Paint _circleInner = Paint()
    ..color = AppColors.blueLight
    ..strokeWidth = 2.sp;

  @override
  void paint(Canvas canvas, Size size) {
    if (points.isNotEmpty && (points.first == points.last)) {
      path.moveTo(points.first.dx, points.first.dy);
      for (int i = 1; i < points.length - 1; i++) {
        path.lineTo(points[i].dx, points[i].dy);
        canvas.drawPath(path, _path);
      }
      path.close();
    }

    for (int i = 0; i < points.length - 1; i++) {
      canvas.drawLine(points[i], points[i + 1], _line);
      canvas.drawCircle(points[i], 10, _circleOuter);
      canvas.drawCircle(points[i], 8, _circleInner);
      canvas.drawCircle(points[i + 1], 10, _circleOuter);
      canvas.drawCircle(points[i + 1], 8, _circleInner);
    }
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}

class GridPainter extends CustomPainter {
  GridPainter({required this.step});

  final double step;
  final _paint = Paint()..color = AppColors.blueAmbient;

  @override
  void paint(Canvas canvas, Size size) {
    for (double x = 0; x <= size.width; x += step) {
      for (double y = 0; y <= size.height; y += step) {
        canvas.drawCircle(Offset(x, y), 5.sp, _paint);
      }
    }
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => false;
}
