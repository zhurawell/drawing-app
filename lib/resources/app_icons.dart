import 'package:flutter/widgets.dart';

class AppIcons {
  AppIcons._();

  static const _kFontFam = 'AppIcons';
  static const String? _kFontPkg = null;

  static const IconData undo = IconData(0xe800, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData redo = IconData(0xe801, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData grid = IconData(0xe802, fontFamily: _kFontFam, fontPackage: _kFontPkg);
}
