import 'package:flutter/material.dart';

class AppColors {
  static const black = Color(0xFF000000);
  static const blueAmbient = Color(0xFF9FCCE6);
  static const blueDark = Color(0xFF377BBB);
  static const blueLight = Color(0xFF0098EE);
  static const grayAmbient = Color(0xFFE3E3E3);
  static const grayDark = Color(0xFF7D7D7D);
  static const grayLight = Color(0xFF7A7A7A);
  static const transparent = Color(0x00000000);
  static const white = Color(0xFFFFFFFF);
}
