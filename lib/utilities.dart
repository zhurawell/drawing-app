import 'dart:math';

import 'package:flutter/material.dart';

extension OffsetStep on Offset {
  Offset round(double step) {
    double dx = (this.dx.round() / step).round() * step;
    double dy = (this.dy.round() / step).round() * step;
    return Offset(dx, dy);
  }
}

/// y = kx + b
class _SectionFuntion {
  Offset point1;
  Offset point2;

  late double k;
  late double b;
  _SectionFuntion(this.point1, this.point2) {
    k = (point1.dy - point2.dy) / (point1.dx - point2.dx);
    b = point1.dy - k * point1.dx;
  }
}

/// Проверка на непересечение отрезков.
bool isIntersection({
  required List<Offset> section1,
  required List<Offset> section2,
}) {
  assert(section1.length == 2, 'Incorrect section1 offsets');
  assert(section2.length == 2, 'Incorrect section2 offsets');

  Offset s1Start;
  Offset s1End;
  Offset s2Start;
  Offset s2End;

  if (min(section1.first.dx, section1.last.dx) <= min(section2.first.dx, section2.last.dx)) {
    s1Start = section1.first;
    s1End = section1.last;
    s2Start = section2.first;
    s2End = section2.last;
  } else {
    s1Start = section2.first;
    s1End = section2.last;
    s2Start = section2.first;
    s2End = section2.last;
  }

  if (s1Start.dx > s1End.dx) {
    s1Start = s1Start + s1End;
    s1End = s1Start - s1End;
    s1Start = s1Start - s1End;
  }
  if (s2Start.dx > s2End.dx) {
    s2Start = s2Start + s2End;
    s2End = s2Start - s2End;
    s2Start = s2Start - s2End;
  }
  // Теперь начало каждого отрезка лежит не правее конца и начало первого отрезка не правее начала правого

  // Если у отрезков нет общих абцисс, то нет и пересечений
  if (s1End.dx < s2Start.dx) {
    return false;
  }

  // Отрезки параллельны оси ординат
  if ((s1Start.dx == s1End.dx) && (s2Start.dx == s2End.dx)) {
    if (s1Start.dx == s2Start.dx) {
      if (!((max(s1Start.dy, s1End.dy) <= min(s2Start.dy, s2End.dy)) ||
          ((max(s2Start.dy, s2End.dy) <= min(s1Start.dy, s1End.dy))))) {
        return true;
      }
    }
    return false;
  }

  _SectionFuntion function1 = _SectionFuntion(s1Start, s1End);
  _SectionFuntion function2 = _SectionFuntion(s2Start, s2End);
  // Один из отрезков вертикальный
  if ((function1.k == double.infinity) || (function2.k == double.infinity)) {
    return function1.k == double.infinity
        ? _isIntersectionSpecial(function1, function2)
        : _isIntersectionSpecial(function2, function1);
  } else {
    return _isIntersectionGlobal(function1, function2);
  }
}

/// Расчёт длины отрезка
double calculateLength(Offset point1, Offset point2) =>
    sqrt(pow(point1.dx - point2.dx, 2) + pow(point1.dy - point2.dy, 2));

/// Проверка на пересечение (оба отрезка невертикальные)
bool _isIntersectionGlobal(_SectionFuntion function1, _SectionFuntion function2) {
  if (function1.k == function2.k) {
    return false; // случай, когда совпадают, не рассматриваю
  }

  double x = (function2.b - function1.b) / (function1.k - function2.k);

  if ((x > max(function1.point1.dx, function2.point1.dx)) &&
      (x < min(function1.point2.dx, function2.point2.dx))) {
    return true;
  } else {
    return false;
  }
}

/// Проверка на пересечение (один из отрезков вертикальный)
bool _isIntersectionSpecial(_SectionFuntion functionVert, _SectionFuntion functionNonVert) {
  double y = functionNonVert.k * functionVert.point1.dx + functionNonVert.b;

  if ((y < max(functionVert.point1.dy, functionVert.point2.dy)) &&
      (y > min(functionVert.point1.dy, functionVert.point2.dy)) &&
      ((min(functionNonVert.point1.dx, functionNonVert.point2.dx) < functionVert.point1.dx) &&
          (max(functionNonVert.point1.dx, functionNonVert.point2.dx) > functionVert.point1.dx))) {
    return true;
  } else {
    return false;
  }
}
