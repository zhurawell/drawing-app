import 'package:flutter/material.dart';

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:drawing_app/floating_buttons.dart';
import 'package:drawing_app/resources/app_colors.dart';
import 'package:drawing_app/painters.dart';
import 'package:drawing_app/utilities.dart';

final drawingProvider = StateProvider<List<Offset>>((ref) => []); // TODO: Map?
final magnetProdiver = StateProvider<bool>((ref) => false);

double magnetStep = 100.sp;

Offset _lastPosition = const Offset(0, 0);
Offset _lastConfirmedPosition = const Offset(0, 0);

class DrawingPage extends ConsumerWidget {
  const DrawingPage({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    List<Offset> drawingOffsets = ref.watch(drawingProvider);
    bool magnetize = ref.watch(magnetProdiver);

    void addPoint() {
      {
        _lastConfirmedPosition = _lastPosition;
        if (ref.read(drawingProvider.notifier).state.isEmpty) {
          _lastConfirmedPosition = _lastPosition;
          ref.read(drawingProvider.notifier).state = [_lastPosition];
        }
        if (_lastConfirmedPosition != ref.read(drawingProvider.notifier).state.last) {
          ref.read(drawingProvider.notifier).state = [...drawingOffsets, _lastConfirmedPosition];
        }

        // FIXME: некорректное определение пересечений по фигуре (по отрезкам нормально)
        /*
        bool noIntersection = true;

        for (int i = 1; i < ref.read(drawingProvider.notifier).state.length - 1; i++) {
          noIntersection = noIntersection &&
              !isIntersection(section1: [
                ref.read(drawingProvider.notifier).state[i - 1],
                ref.read(drawingProvider.notifier).state[i]
              ], section2: [
                ref.read(drawingProvider.notifier).state.last,
                _lastPosition,
              ]);
          if (noIntersection == false) {
            break;
          }
        }

        if (noIntersection == true) {
          _lastConfirmedPosition = _lastPosition;
          if (_lastConfirmedPosition != ref.read(drawingProvider.notifier).state.last) {
            ref.read(drawingProvider.notifier).state = [...drawingOffsets, _lastConfirmedPosition];
          }
        } else {
          drawingOffsets.removeLast();
          _lastConfirmedPosition = drawingOffsets.last;
          ref.read(drawingProvider.notifier).state = drawingOffsets;
        }
        */
      }
    }

    void myPanUpdate(details) {
      _lastPosition = details.globalPosition;
      if (magnetize == true) {
        _lastPosition = _lastPosition.round(magnetStep);
      }

      if (ref.read(drawingProvider.notifier).state.isEmpty) {
        _lastConfirmedPosition = _lastPosition;
        ref.read(drawingProvider.notifier).state = [_lastPosition];
      } else {
        if (ref.read(drawingProvider.notifier).state.length > 1 &&
            ref.read(drawingProvider.notifier).state.last != _lastConfirmedPosition) {
          ref.read(drawingProvider.notifier).state.removeLast();
        }

        if (ref.read(drawingProvider.notifier).state.first !=
            ref.read(drawingProvider.notifier).state.last) {
          ref.read(drawingProvider.notifier).state = [...drawingOffsets, _lastPosition];
        }
      }
    }

    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.centerTop,
      floatingActionButton: const FloatingButtons(),
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        color: AppColors.grayAmbient,
        child: GestureDetector(
          onTapDown: (details) {
            _lastPosition = details.globalPosition;
            if (magnetize == true) {
              _lastPosition = _lastPosition.round(magnetStep);
            }
          },
          onTapUp: (details) => addPoint(),
          onPanUpdate: myPanUpdate,
          onPanEnd: (details) => addPoint(),
          child: CustomPaint(
            size: Size(1080.sp, 2400.sp),
            painter: GridPainter(step: magnetStep),
            foregroundPainter: DrawingPainter(drawingOffsets),
          ),
        ),
      ),
    );
  }
}
